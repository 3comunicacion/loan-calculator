/* Ejecuta el cálculo cuando se escribe */
function xdelay() {
	setTimeout(function(){
        var a, b, c, d;
        a = jQuery('#textA').val();
        b = jQuery('#textC').val();
        c = jQuery('#textC option:selected').text();
        d = jQuery('#textD').val();
        console.log(' a: ' + a + ' b: ' + b + ' c: ' + c + ' d: ' + d);
        calc(a,b,c,d);
    }, 200);
}
/*
 * Cálculo del crédito:
 * a: precio de venta
 * b: coeficiente de interes
 * c: años del crédito
 * d: entrada
 */
function calc(a, b, c, d) {
	var i = b;//(12 * 100);  //interest_rate
	var years = c;
	var down_payment = d;

	var loan = a - down_payment;
	var n = years * 12;
	var one = ( i * Math.pow((1 + i), n) );
	var two = ( Math.pow((1 + i), n) - 1 );
	var result = (loan * one / two);
    result = roundNumber(result, 2);

    if (result == "NaN") { result = ''}
    jQuery('#resultbox').val(result);
}
/* Redondeo */
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);	return result;
}