<?php
/*
Plugin Name: Calculadora de Créditos
Description: Calculadora de Créditos basada en el plugin Advanced Real Estate Mortgage Calculator. Permite definir diferentes coeficientes de crédito por año.
Version: 0.3
Author: Aday Talavera
Author URI: http://www.felfo.com
License: GPL2
*/

/*
    Copyright 2012  Josh Davis (Advanced Real Estate Mortgage Calculator)
    Copyright 2013  Aday Talavera (Modifications and rework in loan-calculator)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function loan_calculator_scripts() {
    wp_enqueue_script('jquery');
    wp_register_script('loan_calculator', plugins_url('script.js', __FILE__));
    wp_enqueue_script('loan_calculator');
    wp_register_style('loan_calculator-style', plugins_url('style.css', __FILE__));
    wp_enqueue_style('loan_calculator-style');
}
add_action('wp_enqueue_scripts', 'loan_calculator_scripts');

/* Initializes default values */
function loan_calculator_settings() {
	$setting_vars = array(
		'loan_calculator_price',
		'loan_calculator_down',
		'loan_calculator_interest',
		'loan_calculator_years',
        'loan_calculator_rate_1',
		'loan_calculator_years_1',
        'loan_calculator_rate_2',
		'loan_calculator_years_2',
        'loan_calculator_rate_3',
		'loan_calculator_years_3',
        'loan_calculator_rate_4',
		'loan_calculator_years_4',
        'loan_calculator_rate_5',
		'loan_calculator_years_5',
        'loan_calculator_rate_6',
		'loan_calculator_years_6',
        'loan_calculator_rate_7',
		'loan_calculator_years_7',
        'loan_calculator_rate_8',
		'loan_calculator_years_8',
        'loan_calculator_rate_9',
		'loan_calculator_years_9',
        'loan_calculator_rate_10',
		'loan_calculator_years_10',
		'loan_calculator_txt_selling_price',
		'loan_calculator_txt_down_payment',
		'loan_calculator_txt_interest_rate',
		'loan_calculator_txt_years',
		'loan_calculator_txt_monthly_payment',
		'loan_calculator_txt_instructions',
		'loan_calculator_txt_money_symbol',
        'loan_calculator_txt_note',
    );
	foreach ( $setting_vars as $setting_var ){
		register_setting( 'loan_calculator_set', $setting_var );
		$cur_value = get_option( $setting_var );
		if ($cur_value === false) {
			if ($setting_var == 'loan_calculator_price'){
				update_option( $setting_var, '12000' );
			}
			elseif ($setting_var == 'loan_calculator_down'){
				update_option( $setting_var, '' );
			}
			elseif ($setting_var == 'loan_calculator_interest'){
				update_option( $setting_var, '4.750' );
			}
			elseif ($setting_var == 'loan_calculator_years'){
				update_option( $setting_var, '5' );
			}
			elseif ($setting_var == 'loan_calculator_txt_selling_price'){
				update_option( $setting_var, 'Precio de venta' );
			}
			elseif ($setting_var == 'loan_calculator_txt_down_payment'){
				update_option( $setting_var, 'Entrada' );
			}
			elseif ($setting_var == 'loan_calculator_txt_interest_rate'){
				update_option( $setting_var, 'Intereseses' );
			}
			elseif ($setting_var == 'loan_calculator_txt_years'){
				update_option( $setting_var, 'Años' );
			}
			elseif ($setting_var == 'loan_calculator_txt_monthly_payment'){
				update_option( $setting_var, 'Mensualidad' );
			}
			elseif ($setting_var == 'loan_calculator_txt_instructions'){
				update_option( $setting_var, 'Introduce los datos que faltan para calcular la mensualidad' );
			}
			elseif ($setting_var == 'loan_calculator_txt_money_symbol'){
				update_option( $setting_var, '€' );
			}
		}
	}
}
add_action( 'admin_init', 'loan_calculator_settings' );

/* Admin menu page */
function loan_calculator_menu() {
	add_options_page( 'Calculadora de Créditos - Configuración', 'Calculadora de Créditos', 'manage_options', 'loan_calculator_uid', 'loan_calculator_options' );
}
function loan_calculator_options() {
	if ( !current_user_can( 'delete_pages' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
?>
<style>
.wrap{font-size: 13px; line-height: 17px;font-family: Arial, sans-serif; color: #000; padding-top: 10px;}
.wrap fieldset{margin:10px 0px; padding:15px; padding-top: 0px; border: 1px solid #ccc;}
.wrap fieldset legend{font-size: 13px; font-weight: bold; margin-left: -5px;}
.wrap fieldset span { font-size:11px; font-style:italic; color: #666;}
.wrap fieldset .entry{margin-top:10px; margin-bottom: 5px;}
.wrap fieldset .fieldleft{display: inline-block; width: 100px; text-align: right; vertical-align:top; margin: 3px 5px 0px 0px;}
.wrap fieldset .fieldright{display: inline-block; width: auto; text-align: left; vertical-align:top; margin: 3px 5px 0px 0px; color: #0066cc;}
.wrap fieldset .marleft{margin-left: 105px; margin-top: -5px;}
.wrap fieldset input{margin-bottom: 4px;}
.wrap fieldset input:first-child{margin-bottom: 0px;}
.wrap fieldset textarea{margin-bottom: 0px;}
.wrap fieldset textarea{width: 300px; height: 80px;}
</style>

<div class="wrap">
    <h2>Calculadora de Créditos - Configuración</h2>
    <form method="post" action="options.php">
        <?php settings_fields('loan_calculator_set'); ?>
        <fieldset>
            <legend>Coeficientes de interés:</legend>
            <p>Los campos en blanco se ignorarán.</p>
            <table class="form-table">
                <?php $idx = array(1, 2, 3, 4, 5, 6, 7 ,8 ,9 ,10); ?>
                <?php foreach ($idx as $i): ?>
                    <div class="entry">
                        <div class="fieldleft">Años</div>
                        <input name="loan_calculator_years_<?php echo $i; ?>" type="text" id="loan_calculator_years<?php echo $i; ?>" size="3" value="<?php echo get_option('loan_calculator_years_'.$i); ?>"/>
                        <div class="fieldleft">Intereses</div>
                        <input name="loan_calculator_rate_<?php echo $i; ?>" type="text" id="loan_calculator_rate<?php echo $i; ?>" size="10" value="<?php echo get_option('loan_calculator_rate_'.$i); ?>"/>%
                    </div>
                <?php endforeach; ?>
            </table>
        </fieldset>
        <fieldset>
            <legend>Datos de entrada por defecto:</legend>
            <table class="form-table">
                <div class="entry">
                    <div class="fieldleft">Precio de venta</div>
                    <input name="loan_calculator_price" type="text" id="loan_calculator_price" size="20" value="<?php echo get_option('loan_calculator_price'); ?>"/>
                    <div class="fieldright">Debe ser un número o un selector de jQuery válido. Por ejemplo #precio</div>
                </div>
                <div class="entry">
                    <div class="fieldleft">Entrada</div>
                    <input name="loan_calculator_down" type="text" id="loan_calculator_down" size="6" value="<?php echo get_option('loan_calculator_down'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Años</div>
                    <input name="loan_calculator_years" type="text" id="loan_calculator_years" size="3" value="<?php echo get_option('loan_calculator_years'); ?>"/>
                </div>
            </table>
        </fieldset>
        <fieldset>
            <legend>Etiquetas por defecto:</legend>
            <table class="form-table">
                <div class="entry">
                    <div class="fieldleft">Precio de venta</div>
                    <input name="loan_calculator_txt_selling_price" type="text" id="loan_calculator_txt_selling_price" size="20" value="<?php echo get_option('loan_calculator_txt_selling_price'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Entrada</div>
                    <input name="loan_calculator_txt_down_payment" type="text" id="loan_calculator_txt_down_payment" size="20" value="<?php echo get_option('loan_calculator_txt_down_payment'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Años</div>
                    <input name="loan_calculator_txt_years" type="text" id="loan_calculator_txt_years" size="20" value="<?php echo get_option('loan_calculator_txt_years'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Mensualidad</div>
                    <input name="loan_calculator_txt_monthly_payment" type="text" id="loan_calculator_txt_monthly_payment" size="20" value="<?php echo get_option('loan_calculator_txt_monthly_payment'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Instrucciones</div>
                    <input name="loan_calculator_txt_instructions" type="text" id="loan_calculator_txt_instructions" size="100" value="<?php echo get_option('loan_calculator_txt_instructions'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Nota</div>
                    <input name="loan_calculator_txt_note" type="text" id="loan_calculator_txt_money_symbol" size="100" value="<?php echo get_option('loan_calculator_txt_note'); ?>"/>
                </div>
                <div class="entry">
                    <div class="fieldleft">Moneda</div>
                    <input name="loan_calculator_txt_money_symbol" type="text" id="loan_calculator_txt_money_symbol" size="3" value="<?php echo get_option('loan_calculator_txt_money_symbol'); ?>"/>
                </div>
            </table>
        </fieldset>
        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
        </p>
    </form>
</div>
<?php
// end function
}
add_action( 'admin_menu', 'loan_calculator_menu' );

/* Calculator form content */
function loan_calculator_markup($class = ''){
	$loan_calculator_price = get_option('loan_calculator_price');
	$loan_calculator_down = get_option('loan_calculator_down');
	//$loan_calculator_interest = get_option('loan_calculator_interest');
	$loan_calculator_years = get_option('loan_calculator_years');
	$loan_calculator_txt_selling_price = get_option('loan_calculator_txt_selling_price');
	$loan_calculator_txt_down_payment = get_option('loan_calculator_txt_down_payment');
	//$loan_calculator_txt_interest_rate = get_option('loan_calculator_txt_interest_rate');
	$loan_calculator_txt_years = get_option('loan_calculator_txt_years');
	$loan_calculator_txt_monthly_payment = get_option('loan_calculator_txt_monthly_payment');
	$loan_calculator_txt_instructions = get_option('loan_calculator_txt_instructions');
    $loan_calculator_txt_note = get_option('loan_calculator_txt_note');
	$loan_calculator_txt_money_symbol = get_option('loan_calculator_txt_money_symbol');

    $idx = array(1, 2, 3, 4, 5, 6, 7 ,8 ,9 ,10);
    $loan_calculator_rates = array();
    foreach ($idx as $i) {
        $years = get_option('loan_calculator_years_'.$i);
        $rate = get_option('loan_calculator_rate_'.$i);
        if (!empty($years) && !empty($rate) && is_numeric($years) && is_numeric($rate)) {
            $loan_calculator_rates[(int)$years] = (float)$rate;
        }
    }

    ob_start();
    ?>
<div class="<?php echo $class; ?>">
    <form id="loan_calculator" action="">
        <div class="loan_calculator_wrapper">
            <span class="loan_calculator_label"><?php echo $loan_calculator_txt_selling_price; ?></span>
            <span class="loan_calculator_field">
                <input name="text" type="text" id="textA" onkeydown="xdelay()" value="<?php echo is_numeric($loan_calculator_price) ? $loan_calculator_price : ''; ?>" size="12" autocomplete="off" />
                <?php echo $loan_calculator_txt_money_symbol; ?>
            </span>
        </div>
        <div class="loan_calculator_wrapper">
            <span class="loan_calculator_label"><?php echo $loan_calculator_txt_down_payment; ?></span>
            <span class="loan_calculator_field">
                <input name="text4" type="text" id="textD" onkeydown="xdelay()" value="<?php echo $loan_calculator_down; ?> " size="12" autocomplete="off"/>
                <?php echo $loan_calculator_txt_money_symbol; ?>
            </span>
        </div>
        <div class="loan_calculator_wrapper">
            <span class="loan_calculator_label"><?php echo $loan_calculator_txt_years; ?></span>
            <span class="loan_calculator_field">
                <select name="text3" id="textC" onchange="xdelay()">
                    <?php foreach ($loan_calculator_rates as $years => $rate): ?>
                    <option value="<?php echo $rate; ?>" <?php echo $loan_calculator_years == $years ? 'selected="selected"' : '';?>><?php echo $years; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>
        <div class="loan_calculator_wrapper">
            <span class="loan_calculator_label"><?php echo $loan_calculator_txt_monthly_payment; ?></span>
            <span class="loan_calculator_field">
                <input name="text5" type="text" id="resultbox" value="" size="12" readonly="readonly" />
                <?php echo $loan_calculator_txt_money_symbol; ?>
            </span>
        </div>
        <div class="loan_calculator_wrapper">
            <span class="loan_calculator_text"><?php echo $loan_calculator_txt_instructions; ?></span>
            <span class="loan_calculator_text"><small><?php echo $loan_calculator_txt_note; ?></small></span>
        </div>
    </form>
</div>
    <?php if (!is_numeric($loan_calculator_price)): ?>
    <script>
        jQuery(document).ready(function(){
            var prize = jQuery('<?php echo $loan_calculator_price; ?>').text();
            jQuery('#textA').val(prize);
        });
    </script>
    <?php endif; ?>
    <?php
    return ob_get_clean();
}

/* Register shortcode */
function loan_calculator_shortcode( $atts ){
	return loan_calculator_markup('loan-calculator-shortcode');
}
add_shortcode('loan-calculator','loan_calculator_shortcode');

/* Register widget */
class loan_calculator_widget extends WP_Widget {
	function loan_calculator_widget() {
		// Instantiate the parent object
		parent::__construct( false, 'Calculadora de Créditos' );
	}
	function widget($args, $instance) {
		// Widget output
		global $loan_calculator_widget_output;
		extract( $args );
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$loan_calculator_widget_output = '<div class="loan_calculator_sidebar">' . loan_calculator_markup('loan-calculator-widget') . '</div>';
	    $loan_calculator_widget_output = $before_widget . $before_title . $title . $after_title . $loan_calculator_widget_output . $after_widget;
		echo $loan_calculator_widget_output;
	}
	function update($new_instance, $old_instance) {
		// Save widget options
        return $new_instance;
	}
	function form($instance) {
		// Output admin widget options form
        $title = strip_tags($instance['title']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title: </label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
		</p>
        <?php
	}
}

function loan_calculator_register_widget() {
	register_widget( 'loan_calculator_widget' );
}
add_action('widgets_init','loan_calculator_register_widget');

?>
